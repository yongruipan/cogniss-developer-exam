const jsonfile = require("jsonfile");
const randomstring = require("randomstring");
const got = require("got");

const inputURL = "http://www.nactem.ac.uk/software/acromine/dictionary.py?sf=";
const outputFile = "output3.json";

var output = {}
console.log("getting 10 acronyms");
output.definitions = [];
console.log("creating looping function")
const getAcronym = async function () {
  // I think recursive does not work well here, so I change it to iterative
  for (let i = 0; i < 10; i++) {
    var acronym = randomstring.generate(3).toUpperCase();
    await got(inputURL + acronym).then(response => {
      console.log("got data for acronym", acronym);
      console.log("add returned data to definitions array");
      output.definitions.push(response.body);
    }).catch(err => {
      console.log(err)
    })
  }

  console.log("saving output file formatted with 2 space indenting");
  jsonfile.writeFile(outputFile, output, { spaces: 2 }, function (err) {
    console.log("All done!");
  });
}
console.log("calling looping function");
getAcronym();
