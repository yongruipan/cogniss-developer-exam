const jsonfile = require("jsonfile");
const randomstring = require("randomstring");
const got = require("got");

const file = 'input2.json'
const outputFile = 'output2.json'

const reverseString = (str) => {
    let newString = "";
    for (let i = str.length - 1; i >= 0; i--) {
        newString += str[i];
    }
    return newString;
}

jsonfile.readFile(file)
    .then(obj => {
        let output = { email: [] }
        obj.names.forEach(name => {
            output.email.push(`${reverseString(name)}${randomstring.generate(5)}@gmail.com`)
        });

        jsonfile.writeFile(outputFile, output, { spaces: 4 })
            .then(res => {
                console.log('Write complete')
            })
            .catch(error => console.error(error))
    })
    .catch(error => console.error(error))
